﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Preferences;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PokeAlert {
	[Activity(Label = "SettingsActivity", Theme = "@style/AppTheme")]
	public class SettingsActivity : Activity {
		protected override void OnCreate(Bundle savedInstanceState) {
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Settings);
			FragmentManager.BeginTransaction().Replace(Resource.Id.frameLayout, new PrefsFragment()).Commit();

			Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			toolbar.Title = "Settings";
			SetActionBar(toolbar);
			toolbar.SetNavigationIcon(Resource.Drawable.ic_pokealert);
		}

		public override bool OnCreateOptionsMenu(Android.Views.IMenu menu) {
			MenuInflater.Inflate(Resource.Menu.settings, menu);
			return base.OnCreateOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(Android.Views.IMenuItem item) {
			if(item.ItemId == Resource.Id.menu_save)
				Finish();
			return base.OnOptionsItemSelected(item);
		}

		public override void OnBackPressed() {
			
		}
	}
}

