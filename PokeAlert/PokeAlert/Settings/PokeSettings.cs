﻿using System;
using System.Collections.Generic;
using PokemonGo.RocketAPI;
using PokemonGo.RocketAPI.Enums;

using Android.Content;
using Android.Preferences;

namespace PokeAlert {
	public class PokeSettings : ISettings{

		private Context AppContext;
		private ISharedPreferences SharedPreferences;
		private ISharedPreferencesEditor SharedPreferencesEditor;

		public PokeSettings(Context appContext) {
			this.AppContext = appContext;
			this.SharedPreferences = PreferenceManager.GetDefaultSharedPreferences(appContext);
			this.SharedPreferencesEditor = SharedPreferences.Edit();
		}

		//Privates
		private AuthType authType = AuthType.Ptc;
		private double defaultAltitude = 50.4;
		private double defaultLatitude = 40.782360;
		private double defaultLongitude = -73.963768;

		private string googleRefreshToken = "";

		//Getters & Setters

		public void SetPokemonEnabled(int id, bool enabled) {
			SharedPreferencesEditor.PutBoolean("pokemonEnabled" + id, enabled);
			SharedPreferencesEditor.Commit();
		}

		public bool IsPokemonEnabled(int id) {
			return SharedPreferences.GetBoolean("pokemonEnabled" + id, true);
		}

		public bool AlertScanEnabled {
			get {
				return SharedPreferences.GetBoolean("alertScanEnabled", true);
			}
			set {
				SharedPreferencesEditor.PutBoolean("alertScanEnabled", value);
				SharedPreferencesEditor.Commit();
			}
		}

		public int AlertScanRadius {
			get {
				return SharedPreferences.GetInt("alertScanRadius", 50);
			}
			set {
				SharedPreferencesEditor.PutInt("alertScanRadius", value);
				SharedPreferencesEditor.Commit();
			}
		}

		public int AlertScanInterval {
			get {
				return SharedPreferences.GetInt("alertScanInterval", 5 * 60);
			}
			set {
				SharedPreferencesEditor.PutInt("alertScanInterval", value);
				SharedPreferencesEditor.Commit();
			}
		}

		public int MapScanMs {
			get {
				return SharedPreferences.GetInt("mapScanMs", 3000);
			}
			set {
				SharedPreferencesEditor.PutInt("mapScanMs", value);
				SharedPreferencesEditor.Commit();
			}
		}

		public int AlertScanMs { 
			get {
				return SharedPreferences.GetInt("alertScanMs", 3000);
			}
			set {
				SharedPreferencesEditor.PutInt("alertScanMs", value);
				SharedPreferencesEditor.Commit();
			}
		}

		public string PtcUsername {
			get {
				#if DEBUG
				return SharedPreferences.GetString("ptcUsername", "DebboR");
				#else
				return SharedPreferences.GetString("ptcUsername", "");
				#endif
			}
			set {
				SharedPreferencesEditor.PutString("ptcUsername", value);
				SharedPreferencesEditor.Commit();
			}
		}

		public string PtcPassword {
			get {
				#if DEBUG
				return SharedPreferences.GetString("ptcPassword", "frieten");
				#else
				return SharedPreferences.GetString("ptcPassword", "");
				#endif
			}
			set {
				SharedPreferencesEditor.PutString("ptcPassword", value);
				SharedPreferencesEditor.Commit();
			}
		}

		public AuthType AuthType {
			get {
				return authType;
			}
		}

		public double DefaultAltitude {
			get {
				return defaultAltitude;
			}
		}

		public double DefaultLatitude {
			get {
				return defaultLatitude;
			}
		}

		public double DefaultLongitude {
			get {
				return defaultLongitude;
			}
		}

		public string GoogleRefreshToken {
			get {
				return googleRefreshToken;
			}

			set {
				googleRefreshToken = value;
			}
		}
	}
}

