﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Preferences;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace PokeAlert {
	public class PrefsFragment : PreferenceFragment, ISharedPreferencesOnSharedPreferenceChangeListener {
		public override void OnCreate(Bundle savedInstanceState) {
			base.OnCreate(savedInstanceState);
			AddPreferencesFromResource(Resource.Xml.Preferences);

			ISharedPreferencesEditor editor = PreferenceManager.GetDefaultSharedPreferences(Application.Context).Edit();
			editor.PutString("pref_key_map_ms", MainActivity.PokeSettings.MapScanMs.ToString());
			editor.PutBoolean("pref_key_alerts_enabled", MainActivity.PokeSettings.AlertScanEnabled);
			editor.PutString("pref_key_alert_rate", (MainActivity.PokeSettings.AlertScanInterval / 60).ToString());
			editor.PutString("pref_key_alert_ms", MainActivity.PokeSettings.AlertScanMs.ToString());
			editor.Commit();
			PreferenceManager.SetDefaultValues(Application.Context, Resource.Xml.Preferences, false);
		}

		public override void OnResume() {
			base.OnResume();
			PreferenceManager.GetDefaultSharedPreferences(Application.Context).RegisterOnSharedPreferenceChangeListener(this);
		}
		public override void OnPause() {
			base.OnPause();
			PreferenceManager.GetDefaultSharedPreferences(Application.Context).UnregisterOnSharedPreferenceChangeListener(this);
		}

		public void OnSharedPreferenceChanged(ISharedPreferences sharedPreferences, string key) {
			MainActivity.PokeSettings.MapScanMs = int.Parse(sharedPreferences.GetString("pref_key_map_ms", "3000"));
			MainActivity.PokeSettings.AlertScanEnabled = sharedPreferences.GetBoolean("pref_key_alerts_enabled", true);
			MainActivity.PokeSettings.AlertScanInterval = 60 * int.Parse(sharedPreferences.GetString("pref_key_alert_rate", "5"));
			MainActivity.PokeSettings.AlertScanMs = int.Parse(sharedPreferences.GetString("pref_key_alert_ms", "3000"));
		}

		public override bool OnPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
			if(preference.Key == "pref_key_alerts_pokemon") {
				StartActivity(new Intent(this.Context, typeof(PokeSelectActivity)));
			}
			return base.OnPreferenceTreeClick(preferenceScreen, preference);
		}
	}
}

