﻿using System;
using Android.App;
using Android.Content;
using Android.Locations;
using Android.Gms.Location;
using Android.Gms.Common.Api;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Gms.Common;

namespace PokeAlert {
	[Service]
	public class LocationProvider : IntentService, GoogleApiClient.IConnectionCallbacks {

		private static LocationProvider instance = null;
		private GoogleApiClient googleApiClient;
		private Context context;
		public static Location Location;

		public LocationProvider() { }

		private LocationProvider(Context context) {
			this.context = context;
			googleApiClient = new GoogleApiClient.Builder(context)
			                                     .AddApi(LocationServices.API)
			                                     .AddConnectionCallbacks(this)
			                                     .Build();
			googleApiClient.Connect();
		}

		public static LocationProvider GetInstance(Context context) {
			if(instance == null)
				instance = new LocationProvider(context);
			return instance;
		}

		public static LocationProvider GetInstance() {
			if(instance == null)
				throw new MethodAccessException("Instance not yet initialised!");
			return instance;
		}

		public void OnConnected(Bundle connectionHint) {
			LocationRequest locationRequest = new LocationRequest();
			locationRequest.SetPriority(LocationRequest.PriorityBalancedPowerAccuracy);
			locationRequest.SetInterval(MainActivity.PokeSettings.AlertScanInterval * 1000);
			locationRequest.SetMaxWaitTime(MainActivity.PokeSettings.AlertScanInterval * 1000);
			LocationServices.FusedLocationApi.RequestLocationUpdates(googleApiClient, locationRequest, PendingIntent.GetService(context, 0, new Intent(context, typeof(LocationProvider)), 0));
			Location = LocationServices.FusedLocationApi.GetLastLocation(googleApiClient);
		}

		public void OnConnectionSuspended(int cause) {}

		protected override void OnHandleIntent(Intent intent) {
			if(LocationResult.HasResult(intent)) {
				Location = LocationResult.ExtractResult(intent).LastLocation;
				if(Location != null)
					Console.WriteLine("Updated location! New location: " + Location.Latitude + ", " + Location.Longitude);
			}
		}
	}
}

