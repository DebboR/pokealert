﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PokeAlert {
	[Activity(Label = "PokeSelectActivity", Theme = "@style/AppTheme")]
	public class PokeSelectActivity : Activity {
		protected override void OnCreate(Bundle savedInstanceState) {
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.PokeSelect);

			Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			SetActionBar(toolbar);
			ActionBar.Title = "Filter Pokemon";
			toolbar.SetNavigationIcon(Resource.Drawable.ic_pokealert);

			ListView listView = FindViewById<ListView>(Resource.Id.listView);
			PokeListAdapter listAdapter = new PokeListAdapter(this);
			listView.Adapter = listAdapter;
		}

		public override bool OnCreateOptionsMenu(Android.Views.IMenu menu) {
			MenuInflater.Inflate(Resource.Menu.settings, menu);
			return base.OnCreateOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(Android.Views.IMenuItem item) {
			if(item.ItemId == Resource.Id.menu_save) 
				Finish();
			return base.OnOptionsItemSelected(item);
		}
	}
}

