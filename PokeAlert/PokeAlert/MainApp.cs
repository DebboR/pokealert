﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;

namespace PokeAlert {
	
	[Application]
	public class MainApp : Application{
		public MainApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer){}

		public override void OnCreate() {
			base.OnCreate();

			LocationProvider.GetInstance(this);
		}
	}
}

