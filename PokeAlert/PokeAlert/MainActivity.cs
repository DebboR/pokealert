﻿using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;

//using Android.Gms.Ads;

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Xamarin;

using PokemonGo.RocketAPI;
using Google.Common.Geometry;

using POGOProtos.Map.Pokemon;

namespace PokeAlert {
	[Activity(Label = "PokeAlert", LaunchMode=Android.Content.PM.LaunchMode.SingleTop, Theme = "@style/AppTheme", MainLauncher = true, Icon = "@mipmap/ic_launcher", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class MainActivity : Activity, IOnMapReadyCallback, GoogleMap.IOnMapClickListener {

		public bool Scanning = false;
		public static PokeSettings PokeSettings;

		private GoogleMap map;
		private Marker scanMarker;
		private Polygon polygon;
		public ProgressBar progressBar;
		public SeekBar seekRadius;
		public Button scanButton;

		private int scanRadius = 100;
		private double lat = 0;
		private double lng = 0;
		private bool loggedIn = false;

		private List<PokeMarker> pokeMarkers = new List<PokeMarker>();
		private System.Timers.Timer pokeMarkerTimer;

		private static Dialog loginDialog;

		protected override void OnCreate(Bundle savedInstanceState) {
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Main);

			if(PokeSettings == null)
				PokeSettings = new PokeSettings(Application.Context);

			//MobileAds.Initialize(Application.Context, GetString(Resource.String.ad_app_id));

			FragmentManager.FindFragmentById<MapFragment>(Resource.Id.map).GetMapAsync(this);

			scanButton = FindViewById<Button>(Resource.Id.btnScan);
			scanButton.Enabled = false;
			scanButton.Click += (sender, e) => {
				if(Scanning) {
					Scanning = false;
					scanButton.Text = "Scan";
				} else {
					Scanning = true;
					scanButton.Text = "Cancel";
					new Task(Scan).Start();
				}
			};

			TextView txtSearchRadius = FindViewById<TextView>(Resource.Id.txtSearchRadius);

			seekRadius = FindViewById<SeekBar>(Resource.Id.seekRadius);
			seekRadius.ProgressChanged += (sender, e) => {
				txtSearchRadius.Text = e.Progress + "m";
				scanRadius = e.Progress;
				updateMapMarkers();
			};

			progressBar = FindViewById<ProgressBar>(Resource.Id.progressBar);
			progressBar.Indeterminate = false;

			pokeMarkerTimer = new System.Timers.Timer(1000);
			pokeMarkerTimer.Elapsed += (sender, e) => {
				foreach(PokeMarker pm in pokeMarkers)
					pm.Tick(this);
				pokeMarkers.RemoveAll((PokeMarker obj) => obj.TimeLeft < 0);
			};
			pokeMarkerTimer.Enabled = true;

			Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			SetActionBar(toolbar);
			ActionBar.Title = "PokéAlert";
			toolbar.SetNavigationIcon(Resource.Drawable.ic_pokealert);

			//AdView adView = FindViewById<AdView>(Resource.Id.adView);
			//adView.LoadAd(new AdRequest.Builder().AddTestDevice(AdRequest.DeviceIdEmulator).Build());
		}

		protected override void OnResume() {
			base.OnResume();
		}

		public override bool OnCreateOptionsMenu(Android.Views.IMenu menu) {
			MenuInflater.Inflate(Resource.Menu.home, menu);
			return base.OnCreateOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(Android.Views.IMenuItem item) {
			if(item.ItemId == Resource.Id.menu_settings)
				StartActivity(new Intent(this, typeof(SettingsActivity)));
			return base.OnOptionsItemSelected(item);
		}

		public void OnMapReady(GoogleMap googleMap) {
			map = googleMap;

			map.MyLocationEnabled = true;
			map.UiSettings.MapToolbarEnabled = false;
			map.SetOnMapClickListener(this);

			StartLogin();
		}

		private void StartLogin() {
			Task t = new Task(Login);
			t.Start();
			t.Wait();
		}

		private async void Login() {
			ProgressDialog progressDialog = null;
			RunOnUiThread(() => {
				progressDialog = ProgressDialog.Show(this, "", "Logging in...");
			});

			bool success = await PokeFunctions.Login();

			RunOnUiThread(() => {
				progressDialog.Dismiss();
				Toast.MakeText(this, success ? "Login successful!" : "Login failed!", ToastLength.Long).Show();
				loggedIn = success;
				if(!success) {
					showLoginDialog(this);
				} else {
					scanButton.Enabled = true;
				}
			});

			if(PokeSettings.AlertScanEnabled) {
				AlarmManager alarmManager = (AlarmManager)GetSystemService(Context.AlarmService);
				Intent restartIntent = new Intent(CreatePackageContext(PackageName, 0), typeof(PokeService));
				PendingIntent pendingRestartIntent = PendingIntent.GetService(CreatePackageContext(PackageName, 0), 66, restartIntent, 0);
				alarmManager.SetInexactRepeating(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime(), MainActivity.PokeSettings.AlertScanInterval * 1000, pendingRestartIntent);
			}
		}

		private async void Scan() {
			await PokeFunctions.GetPokemons(lat, lng, scanRadius, PokeSettings.MapScanMs, progressBar, this);
		}

		public void placePokemon(List<WildPokemon> list) {
			RunOnUiThread(() => {
				// Draw pokemon
				foreach(WildPokemon pokemon in list) {
					MarkerOptions mOptions = new MarkerOptions();
					mOptions.SetIcon(Pokemon.GetIcon(Pokemon.GetID(pokemon.PokemonData.PokemonId.ToString()), 180, this));
					mOptions.SetPosition(new LatLng(pokemon.Latitude, pokemon.Longitude));
					pokeMarkers.Add(new PokeMarker(map.AddMarker(mOptions), pokemon.PokemonData.PokemonId.ToString(), (int) (pokemon.TimeTillHiddenMs / (long) 1000)));
				}
			});
		}

		public void OnMapClick(LatLng point) {
			if(!Scanning) {
				lat = point.Latitude;
				lng = point.Longitude;
				updateMapMarkers();
			}
		}

		private void updateMapMarkers() {
			LatLng point = new LatLng(lat, lng);
			if(scanMarker == null)
				scanMarker = map.AddMarker(new MarkerOptions().SetPosition(point).SetIcon(BitmapDescriptorFactory.FromBitmap(
					Bitmap.CreateScaledBitmap(((BitmapDrawable)Resources.GetDrawable(Resource.Drawable.marker)).Bitmap, 60, 96, false)
				)));
			else
				scanMarker.Position = point;

			if(polygon != null)
				polygon.Remove();

			PolygonOptions po = new PolygonOptions()
				.Add(Util.GetOffset(point, -1 * scanRadius, scanRadius))
				.Add(Util.GetOffset(point, scanRadius, scanRadius))
				.Add(Util.GetOffset(point, scanRadius, -1 * scanRadius))
				.Add(Util.GetOffset(point, -1 * scanRadius, -1 * scanRadius))
				.InvokeFillColor(Android.Graphics.Color.Argb(100, 238, 64, 37))
				.InvokeStrokeColor(Android.Graphics.Color.Argb(200, 238, 64, 37))
				.InvokeStrokeWidth(5);
			polygon = map.AddPolygon(po);
		}

		private static void showLoginDialog(MainActivity activity) {
			View loginDialogView = activity.LayoutInflater.Inflate(Resource.Layout.LoginDialog, null);

			Button btnLogin = loginDialogView.FindViewById<Button>(Resource.Id.btn_login);
			EditText txtName = loginDialogView.FindViewById<EditText>(Resource.Id.txt_name);
			EditText txtPassword = loginDialogView.FindViewById<EditText>(Resource.Id.password);

			txtName.Text = PokeSettings.PtcUsername;
			txtPassword.Text = PokeSettings.PtcPassword;

			btnLogin.Click += (sender, e) => {
				if(txtName.Text != "" && txtPassword.Text != "") {
					Console.WriteLine("Login");
					PokeSettings.PtcUsername = txtName.Text;
					PokeSettings.PtcPassword = txtPassword.Text;
					activity.StartLogin();
					loginDialog.Dismiss();
				} else {
					Toast.MakeText(activity, "Username and password cannot be empty!", ToastLength.Short).Show();
				}
			};

			AlertDialog.Builder loginDialogBuilder = new AlertDialog.Builder(activity);
			loginDialogBuilder.SetTitle("Login");
			loginDialogBuilder.SetView(loginDialogView);
			loginDialog = loginDialogBuilder.Create();
			loginDialog.Show();
		}
	}
}


