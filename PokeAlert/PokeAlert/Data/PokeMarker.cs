﻿using System;
using System.Timers;

using Android.Gms.Maps;
using Android.Gms.Maps.Model;

using Android.App;

namespace PokeAlert {
	public class PokeMarker {
		public Marker Marker;
		public int TimeLeft;
		public string PokeName;

		public PokeMarker(Marker marker, string pokeName, int timeLeft) {
			this.Marker = marker;
			this.PokeName = pokeName;
			this.TimeLeft = timeLeft;

			marker.Title = getTitle();
		}

		public void Tick(Activity activity) { 
			TimeLeft--;
			activity.RunOnUiThread(() => {
				Marker.Title = getTitle();
				if(Marker.IsInfoWindowShown) {
					Marker.HideInfoWindow();
					Marker.ShowInfoWindow();
				}
				if(TimeLeft < 0) {
					Marker.Remove();
					Marker.Dispose();
				}
			});
		}

		private string getTitle() {
			return PokeName + ": " + TimeLeft/60 + "m" + (TimeLeft%60).ToString("00") + "s";
		}
	}
}

