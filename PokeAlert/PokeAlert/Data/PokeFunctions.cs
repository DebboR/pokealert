﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using PokemonGo.RocketAPI;
using Google.Common.Geometry;

using POGOProtos.Map.Pokemon;

using Android.App;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;

namespace PokeAlert {
	public class PokeFunctions {

		private static Random random = new Random();

		private const int POKERADIUS = 50;

		private static Client client;

		public static async Task<bool> Login() {
			try {
				client = new Client(MainActivity.PokeSettings);
				await client.Login.DoPtcLogin(MainActivity.PokeSettings.PtcUsername, MainActivity.PokeSettings.PtcPassword);
			} catch {
				return false;
			}
			return true;
		}

		public static async Task<List<WildPokemon>> GetPokemons(double lat, double lng){
			await SetLocation(lat, lng);
			var mapObjects = await client.Map.GetMapObjects();
			List<WildPokemon> wildPokemonList = new List<WildPokemon>();
			foreach(var mapCell in mapObjects.MapCells) {
				foreach(var wildPokemon in mapCell.WildPokemons) {
					Console.WriteLine(Pokemon.GetID(wildPokemon.PokemonData.PokemonId.ToString()) + " " + wildPokemon.PokemonData.PokemonId + ": " + wildPokemon.TimeTillHiddenMs / 1000 + "s, Lat: " + wildPokemon.Latitude + ", Long: " + wildPokemon.Longitude);
					if(Pokemon.GetID(wildPokemon.PokemonData.PokemonId.ToString()) != -1)
						wildPokemonList.Add(wildPokemon);
				}
			}
			return wildPokemonList;
		}

		public static async Task GetPokemons(double lat, double lng, int radius, int delay, ProgressBar progressBar, MainActivity activity) {
			int n = (int) Math.Ceiling((double) radius / POKERADIUS);

			activity.RunOnUiThread(() => {
				progressBar.Max = n * n;
				progressBar.Progress = 0;
			});

			int dist = n == 1 ? 0 : (radius * 2) / (n - 1);

			LatLng startPoint = Util.GetOffset(new LatLng(lat, lng), -0.5 * n * dist, -0.5 * n * dist);

			for(int i = 0; i < n; i++) {
				for(int j = 0; j < n; j++) {
					if(!activity.Scanning)
						return;
					LatLng pos = Util.GetOffset(startPoint, i * dist, j * dist);
					for(int retry = 0; retry < 5; retry++) {
						try {
							Console.WriteLine("Scanning at " + i + " " + j + ": " + pos.Latitude + " " + pos.Longitude);
							Console.WriteLine("Try #" + (retry + 1));
							Thread.Sleep(delay);
							activity.placePokemon(await GetPokemons(pos.Latitude, pos.Longitude));
							break;
						} catch {};
					}
					activity.RunOnUiThread(() => {
						progressBar.Progress = i*n + j;
					});
				}
			}

			activity.RunOnUiThread(() => {
				activity.Scanning = false;
				activity.scanButton.Text = "Scan";
			});
		}

		public static async Task<List<WildPokemon>> GetPokemonList(double lat, double lng, int radius, int delay) {
			List<WildPokemon> wildPokemonList = new List<WildPokemon>();

			int n = (int)Math.Ceiling((double)radius / POKERADIUS);

			int dist = n == 1 ? 0 : (radius * 2) / (n - 1);

			LatLng startPoint = Util.GetOffset(new LatLng(lat, lng), -0.5 * n * dist, -0.5 * n * dist);

			for(int i = 0; i < n; i++) {
				for(int j = 0; j < n; j++) {
					LatLng pos = Util.GetOffset(startPoint, i * dist, j * dist);
					for(int retry = 0; retry < 5; retry++) {
						try {
							Console.WriteLine("Scanning at " + i + " " + j + ": " + pos.Latitude + " " + pos.Longitude);
							Console.WriteLine("Try #" + (retry + 1));
							Thread.Sleep(delay);
							wildPokemonList.AddRange(await GetPokemons(pos.Latitude, pos.Longitude));
							break;
						} catch { };
					}
				}
			}
			return wildPokemonList;
		}

		private static async Task SetLocation(double lat, double lng) {
			await client.Player.UpdatePlayerLocation(lat, lng, random.NextDouble() * 300);
		}
	}
}

