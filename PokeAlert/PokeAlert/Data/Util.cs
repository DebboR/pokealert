﻿using System;

using Android.Gms.Maps.Model;

namespace PokeAlert {
	public class Util {

		public static LatLng GetOffset(LatLng from, double offsetX, double offsetY) {
			double lat = from.Latitude + (offsetX / 111111);
			double lng = from.Longitude + (offsetY / (111111 * Math.Cos(lat * Math.PI / 180)));
			return new LatLng(lat, lng);
		}

	}
}

