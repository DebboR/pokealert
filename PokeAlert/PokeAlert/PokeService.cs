﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;

using POGOProtos.Map.Pokemon;

namespace PokeAlert {

	[Service]
	public class PokeService : Service {

		private const int RequestCode = 666;

		public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId) {
			scan();
			return StartCommandResult.Sticky;
		}

		private async void scan() {
			try {
				Console.WriteLine("Starting background scan");
				if(LocationProvider.Location != null)
					sendNotification(await PokeFunctions.GetPokemonList(LocationProvider.Location.Latitude, LocationProvider.Location.Longitude, MainActivity.PokeSettings.AlertScanRadius, MainActivity.PokeSettings.AlertScanMs));
			} catch {
				Console.WriteLine("Scanning created exception!");
			}
		}

		private void sendNotification(List<WildPokemon> pokemonList) {
			List<WildPokemon> filteredPokemonList = new List<WildPokemon>();
			foreach(WildPokemon wp in pokemonList)
				if(MainActivity.PokeSettings.IsPokemonEnabled(wp.PokemonData.PokemonId.GetHashCode()))
					filteredPokemonList.Add(wp);

			Notification.Builder builder = new Notification.Builder(CreatePackageContext(PackageName, 0))
				.SetContentTitle("PokéAlert")
				.SetSmallIcon(Resource.Drawable.ic_pokealert)
				.SetPriority(NotificationPriority.High.GetHashCode())
				.SetContentIntent(PendingIntent.GetActivity(this, 0, new Intent(Application.Context, typeof(MainActivity)), PendingIntentFlags.UpdateCurrent));

			if(pokemonList.Count == 0) {
				builder.SetContentText("No pokemon found near you");
			} else {
				Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
				inboxStyle.SetBigContentTitle("Nearby pokemon:");
				foreach(WildPokemon wp in filteredPokemonList)
					inboxStyle.AddLine(Pokemon.POKEMON[wp.PokemonData.PokemonId.GetHashCode()]);
				builder.SetStyle(inboxStyle);
			}
			
			NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
			notificationManager.Notify(RequestCode, builder.Build());
		}

		public override void OnDestroy() {
			base.OnDestroy();
		}

		public override IBinder OnBind(Intent intent) {
			return null;
		}
	}
}

