﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace PokeAlert {
	public class PokeListAdapter : BaseAdapter{
		private Activity activity;

		public PokeListAdapter(Activity activity) {
			this.activity = activity;
		}

		public override int Count {
			get {
				return Pokemon.POKEMON.Length;
			}
		}

		public override Java.Lang.Object GetItem(int position) {
			return Pokemon.GetID(Pokemon.POKEMON[position]);
		}

		public override long GetItemId(int position) {
			return Pokemon.GetID(Pokemon.POKEMON[position]);
		}

		public override View GetView(int position, View convertView, ViewGroup parent) {
			if(convertView == null)
				convertView = ((LayoutInflater)activity.GetSystemService(Service.LayoutInflaterService)).Inflate(Resource.Layout.PokeListItem, parent, false);

			TextView title = convertView.FindViewById<TextView>(Resource.Id.textView);
			ImageView image = convertView.FindViewById<ImageView>(Resource.Id.imageView);
			CheckBox check = convertView.FindViewById<CheckBox>(Resource.Id.checkBox);

			title.Text = Pokemon.POKEMON[position];
			title.SetTextAppearance(Android.Resource.Style.TextAppearanceLarge);

			image.SetImageDrawable(Pokemon.GetDrawable(Pokemon.GetID(Pokemon.POKEMON[position]), activity));

			check.Checked = MainActivity.PokeSettings.IsPokemonEnabled((int)GetItemId(position));
			check.CheckedChange += (sender, e) => {
				MainActivity.PokeSettings.SetPokemonEnabled((int)GetItemId(position), check.Checked);
			};

			return convertView;
		}
	}
}

